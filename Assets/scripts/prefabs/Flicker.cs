﻿using UnityEngine;

public class Flicker : MonoBehaviour
{
    private Animator Anim;
    private float TimeLeft;
    public float rFloat;

    // Start is called before the first frame update
    void Start()
    {
        Anim = GetComponent<Animator>();
        rFloat = Random.Range(5f, 10f);
        TimeLeft = rFloat;
    }

    // Update is called once per frame
    void Update()
    {
        if(Anim.GetBool("Flick_Triggerd") == true) { GetNumber(); }
        FlickTimer();
    }

    void GetNumber()
    {
        rFloat = Random.Range(5f, 10f);
        TimeLeft = rFloat;
        Anim.SetBool("Flick_Triggerd", false);
    }

    void FlickTimer()
    {
        TimeLeft -= Time.deltaTime;
        if(TimeLeft <= 0)
        {
            Anim.SetTrigger("flick");
            Anim.SetBool("Flick_Triggerd", true);
        }
    }
}
