﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : MonoBehaviour
{
    [SerializeField]
    public float maxHealth;

    [SerializeField]
    private GameObject
        deathChunkParticle,
        deathBloodParticle;

    public float currentHealth = 10f;

    private GameManager GM;

    public healthbar healthbar;

    private void Start()
    {
        currentHealth = maxHealth;
        GM = GameObject.Find("GameManager").GetComponent<GameManager>();
        healthbar = GameObject.Find("Canvas").GetComponentInChildren<healthbar>();
        healthbar.SetMaxHealth(maxHealth);
    }

    public void DecreaseHealth(float amount)
    {
        currentHealth -= amount;

        healthbar.SetHealth(currentHealth);

        if(currentHealth <= 0.0f)
        {
            Die();
        }
    }

    private void Die()
    {
        GM.Respawn();
        Destroy(gameObject);
    }


}
