﻿using UnityEngine;

public class CombatSystem : MonoBehaviour
{
    [SerializeField]
    private bool CombatEnabled;
    [SerializeField]
    private float InputTimer, attack1Radius, attack1Damage;
    [SerializeField]
    private Transform attack1HitBoxPos;
    [SerializeField]
    private LayerMask whatIsDamageable;

    public bool gotInput,  isFirstAttack, isSecondAttack;
    public bool isAttacking = false;

    private float lastInputTime = Mathf.NegativeInfinity;
    private AttackDetails AttackDetails;

    private Animator Anim;

    private PlayerStats PS;

    private void Start()
    {
        Anim = GetComponent<Animator>();
        Anim.SetBool("canAttack", CombatEnabled);
        PS = GetComponent<PlayerStats>();
    }
    private void Update()
    {
        CheckCombatInput();
        CheckAttacks();
    }

    private void CheckCombatInput()
    {
        if (Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.Space))
        {
            if (CombatEnabled)
            {
                //attempt combat    
                gotInput = true;
                lastInputTime = Time.time;
            }
        }
    }

    void CheckAttacks()
    {
        if (gotInput)
        {
            //perform attack
            if (!isAttacking)
            {
                gotInput = false;
                isAttacking = true;
                isFirstAttack = !isFirstAttack;
                Anim.SetBool("attack1", true);
                Anim.SetBool("firstAttack", isFirstAttack);
                Anim.SetBool("isAttacking", isAttacking);
            }
        }

        if (Time.time >= lastInputTime + InputTimer)
        {
            gotInput = false;
        }
    }

    void CheckAttackHitBox()
    {
        Collider2D[] detectedObjects = Physics2D.OverlapCircleAll(attack1HitBoxPos.position, attack1Radius, whatIsDamageable);

        Debug.Log("HIT");
        AttackDetails.damageAmount = attack1Damage;
        AttackDetails.position = transform.position;

        foreach (Collider2D collider in detectedObjects)
        {
            collider.transform.parent.SendMessage("Damage", AttackDetails);
            // instantiate hit particle
        }
    }

    private void FinishAttack1()
    {
        isAttacking = false;
        Anim.SetBool("isAttacking", isAttacking);
        Anim.SetBool("attack1", false);
    }

    private void Damage(AttackDetails attackDetails)
    {
        int direction;

        PS.DecreaseHealth(attackDetails.damageAmount);

        if(attackDetails.position.x < transform.position.x)
        {
            direction = 1;
        }
        else
        {
            direction = -1;
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(attack1HitBoxPos.position, attack1Radius);
    }



}
