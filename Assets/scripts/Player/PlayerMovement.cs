﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] float speed = 1f;
    [SerializeField] float jumpForce = 2f;

    public Animator Anim;
    public Rigidbody2D rb;
    public GameObject AP;

    public bool isGrounded;

    private bool isDead = false;

    private float inCombat;
    public float CombatTimer;

    private Vector3 adjustment;

    // Start is called before the first frame update
    void Start()
    {
        Anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        //Get Input
        float inputX = Input.GetAxis("Horizontal");

        //Direction Swap
        if(inputX > 0)
        {
            GetComponent<SpriteRenderer>().flipX = true;

        }
        else if (inputX < 0)
        {
            GetComponent<SpriteRenderer>().flipX = false;
        }

        //Move
        rb.velocity = new Vector2(inputX * speed, rb.velocity.y);

        //----Animation Controller----
        //Death
        if (Input.GetKeyDown("e"))
        {
            if (!isDead) { Anim.SetTrigger("Death"); }
            else { Anim.SetTrigger("Recover"); }
            isDead = !isDead;
        }

        //move
        else if (Mathf.Abs(inputX) > Mathf.Epsilon)
        { Anim.SetInteger("Movement", 1); }

        //jump
        else if (Input.GetKeyDown("space") && isGrounded)
        {
            Anim.SetTrigger("Jump");
            isGrounded = false;
            Anim.SetBool("Grounded", isGrounded);
            rb.velocity = new Vector2(rb.velocity.x, jumpForce);
        }
        
        //Idle
        else { Anim.SetInteger("Movement", 0); }
        
    }
}
