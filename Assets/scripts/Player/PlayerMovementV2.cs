﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerMovementV2 : MonoBehaviour
{
    //----------------Children------------
    public Animator anim;
    public SpriteRenderer SR;

    //----------Parent-------------
    public Rigidbody2D rb;

    public float speed = 2f;

    private Vector3 adjustment;

    private CombatSystem CS;

    private bool isAttacking = false;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        SR = GetComponentInChildren<SpriteRenderer>();
        rb = GetComponent<Rigidbody2D>();
        CS = GetComponent<CombatSystem>();

    }

    // Update is called once per frame
    void Update()
    {
        float InputX = Input.GetAxis("Horizontal");



        //Movement
        rb.velocity = new Vector2(InputX * speed, rb.velocity.y);

        //Move
        if (Mathf.Abs(InputX) > Mathf.Epsilon)
        { anim.SetInteger("Movement", 1); }
        //Idle
        else { anim.SetInteger("Movement", 0); }
    }

    private void LateUpdate()
    {
        if (Input.GetKeyDown(KeyCode.D) && adjustment.x != 1)
        {
            adjustment = transform.localScale;
            adjustment.x *= -1;
            transform.localScale = adjustment;
        }
        else if (Input.GetKeyDown(KeyCode.A) && adjustment.x != -1)
        {
            adjustment = transform.localScale;
            adjustment.x *= -1;
            transform.localScale = adjustment;
        }
        else if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene(0);
        }
    }
}
