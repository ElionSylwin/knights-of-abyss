﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "newPlayerDetectedData", menuName = "Data/State Data/Player Detected state")]
public class D_PlayerDetected : ScriptableObject
{
    public float LongRangeActionTime = 1.5f;
}
