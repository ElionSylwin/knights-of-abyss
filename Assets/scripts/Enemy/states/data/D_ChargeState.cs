﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "newChargeData", menuName = "Data/State Data/Charge state")]
public class D_ChargeState : ScriptableObject
{
    public float chargeSpeed = 5f;

    public float chargeTime = 5f;
}
