﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class Entity : MonoBehaviour
{
    public int FacingDirection { get; private set; }

    public D_Entity entityData;

    public FiniteStateMachine stateMachine;
    public Rigidbody2D rb { get; private set; }
    public Animator Anim { get; private set; }
    public GameObject aliveGO { get; private set; }
    public AnimationToStateMachine atsm { get; private set; }

    [SerializeField]
    private Transform wallCheck;
    [SerializeField]
    private Transform ledgeCheck;
    [SerializeField]
    private Transform PlayerCheck;

    protected bool isDead = false;

    private float currentHeath;

    private int lastDamageDirection;

    private Vector2 velocityWorkspace;

    public virtual void Start()
    {
        FacingDirection = 1;
        currentHeath = entityData.maxHealth;

        aliveGO = transform.Find("EnemyGFX").gameObject;
        rb = aliveGO.GetComponent<Rigidbody2D>();
        Anim = aliveGO.GetComponent<Animator>();
        atsm = aliveGO.GetComponent<AnimationToStateMachine>();

        stateMachine = new FiniteStateMachine();
    }

    public virtual void Update()
    {
        stateMachine.CurrentState.LogicUpdate();
    }

    public virtual void FixedUpdate()
    {
        stateMachine.CurrentState.PhysicsUpdate();
    }

    public virtual void SetVelocity(float velocity)
    {
        velocityWorkspace.Set(FacingDirection * velocity, rb.velocity.y);
        rb.velocity = velocityWorkspace;
    }

    public virtual bool CheckWall()
    {
        return Physics2D.Raycast(wallCheck.position, aliveGO.transform.right, entityData.wallCheckDistance, entityData.whatisGround);
    }

    public virtual void DamageHop(float velocity)
    {
        velocityWorkspace.Set(rb.velocity.x, velocity);
        rb.velocity = velocityWorkspace;
    }

    public virtual void Damage(AttackDetails attackDetails)
    {
        currentHeath -= attackDetails.damageAmount;

        DamageHop(entityData.damageHopSpeed);

        if(attackDetails.position.x > aliveGO.transform.position.x)
        {
            lastDamageDirection = -1;
        }
        else
        {
            lastDamageDirection = 1;
        }

        if(currentHeath <= 0)
        {
            isDead = true;
        }
    }

    public virtual bool CheckLedge()
    {
        return Physics2D.Raycast(ledgeCheck.position, Vector2.down, entityData.ledgeCheckDistance, entityData.whatisGround);
    }

    public virtual void Flip()
    {
        FacingDirection *= -1;
        aliveGO.transform.Rotate(0f, 180f, 0f);
    }

    public virtual bool CheckPlayerInMinAgroRange()
    {
        return Physics2D.Raycast(PlayerCheck.position, aliveGO.transform.right, entityData.MinAgroDistance, entityData.whatisPlayer);
    }

    public virtual bool CheckPlayerInMaxAgroRange()
    {
        return Physics2D.Raycast(PlayerCheck.position, aliveGO.transform.right, entityData.MaxAgroDistance, entityData.whatisPlayer);
    }

    public virtual bool CheckPlayerInCloseRangeAction()
    {
        return Physics2D.Raycast(PlayerCheck.position, aliveGO.transform.right, entityData.closeRangeActionDistance, entityData.whatisPlayer);
    }

    public virtual void OnDrawGizmos()
    {
        Gizmos.DrawLine(wallCheck.position, wallCheck.position + (Vector3)(Vector2.right * FacingDirection * entityData.wallCheckDistance));
        Gizmos.DrawLine(ledgeCheck.position, ledgeCheck.position + (Vector3)(Vector2.down * entityData.ledgeCheckDistance));

        Gizmos.DrawWireSphere(PlayerCheck.position + (Vector3)(Vector2.right * entityData.closeRangeActionDistance), 0.2f);
        Gizmos.DrawWireSphere(PlayerCheck.position + (Vector3)(Vector2.right * entityData.MinAgroDistance), 0.2f);
        Gizmos.DrawWireSphere(PlayerCheck.position + (Vector3)(Vector2.right * entityData.MaxAgroDistance), 0.2f);
    }
}
