﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScripts : MonoBehaviour
{
    public float Health;
    public float BaseHealth;
    public float MaxHealth;
    public float level;

    private CombatSystem CS;

    // Start is called before the first frame update
    void Start()
    {
        CS = GameObject.Find("Player_GFX").GetComponent<CombatSystem>();

        MaxHealth = BaseHealth + (BaseHealth * level) % 10;
        Health = MaxHealth;
    }

    private void Damage(float amount)
    {
        Health -= amount;
        Debug.Log("HIT");
    
    if(Health <= 0.0f)
        {
            
        }
    
    }

}


