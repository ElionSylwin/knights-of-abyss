﻿using UnityEngine;

public class EnemyBehaviourSystem : MonoBehaviour
{
    private enum State
    {
        walking,
        knockback, 
        dead
    }

    private State currentstate;

    private bool groundDetected, wallDetected;

    [SerializeField]
    private Transform groundCheck, wallCheck, touchDamageCheck;
    [SerializeField]
    private LayerMask WhatIsGround, WhatIsPlayer;
    [SerializeField]
    private float groundCheckDistance, wallCheckDistance, movementSpeed, maxHealth, knockbackDuration, lastTouchDamageTime, touchDamageCooldown,
                  touchDamage, touchDamageWidth, touchDamageHeight;
    [SerializeField]
    private Vector2 knockbackSpeed;

    private int facingDirection, damageDirection;

    private float currentHealth, knockbackStartTime;

    private float[] attackDetails = new float[2];

    private Vector2 movement, touchDamageBotLeft, touchDamageTopRight;

    private GameObject Enemy;
    private Rigidbody2D Enemyrb;
    private Animator EnemyAnim;

    private void Start()
    {
        Enemy = transform.Find("EnemyGFX").gameObject;
        Enemyrb = Enemy.GetComponent<Rigidbody2D>();
        EnemyAnim = Enemy.GetComponent<Animator>();

        facingDirection = -1;
        currentHealth = maxHealth;
    }
    private void Update()
    {
        switch(currentstate)
        {
            case State.walking:
                UpdateWalkingState();
                break;
            case State.knockback:
                UpdateKnockbackState();
                break;
            case State.dead:
                UpdateDeadState();
                break;
        }
    }

    //----WALKING STATE---
    private void EnterWalkingState()
    {

    }

    private void UpdateWalkingState()
    {
        groundDetected = Physics2D.Raycast(groundCheck.position, Vector2.down, groundCheckDistance, WhatIsGround);
        wallDetected = Physics2D.Raycast(wallCheck.position, Vector2.right, wallCheckDistance, WhatIsGround);

        CheckTouchDamage();

        if(!groundDetected || wallDetected)
        {
            Flip();
        }
        else
        {
            movement.Set(movementSpeed * facingDirection, Enemyrb.velocity.y);
            Enemyrb.velocity = movement;
        }
    }

    private void ExitWalkingState()
    {

    }

    //------------KNOCKBACK STATE----------------------
    private void EnterKnockbackState()
    {
        knockbackStartTime = Time.time;
        movement.Set(knockbackSpeed.x * damageDirection, knockbackSpeed.y);
        Enemyrb.velocity = movement;
        EnemyAnim.SetBool("knockback", true);
    }

    private void UpdateKnockbackState()
    {
        if(Time.time >= knockbackStartTime + knockbackDuration)
        {
            SwitchState(State.walking);
        }
    }

    private void ExitKnockbackState()
    {
        EnemyAnim.SetBool("knockback", false);
    }
    //---------------DEAD STATE--------------------
    private void EnterDeadState()
    {
        EnemyAnim.SetBool("Dead", true);
        Destroy(gameObject);
    }

    private void UpdateDeadState()
    {

    }

    private void ExitDeadState()
    {

    }


    //---------Other Functions-------------
    private void SwitchState(State state)
    {
        switch (currentstate)
        {
            case State.walking:
                ExitWalkingState();
                break;
            case State.knockback:
                ExitKnockbackState();
                break;
            case State.dead:
                ExitDeadState();
                break;

        }

        switch (state)
        {
            case State.walking:
                EnterWalkingState();
                break;
            case State.knockback:
                EnterKnockbackState();
                break;
            case State.dead:
                EnterDeadState();
                break;

        }

        currentstate = state;
    }

    private void Flip()
    {
        facingDirection *= -1;
        Enemy.transform.Rotate(0.0f, 180.0f, 0.0f);
    }

    private void Damage(float[] AttackDetails)
    {
        currentHealth -= AttackDetails[0];
        if(AttackDetails[1] > Enemy.transform.position.x)
        {
            damageDirection = -1;
        }
        else
        {
            damageDirection = 1;
        }

        //instantiate hit particles

        if (currentHealth > 0.0f)
        {
            SwitchState(State.knockback);
        }
        else if (currentHealth < 0.0f)
        {
            SwitchState(State.dead);
        }
    }

    private void CheckTouchDamage()
    {
        if(Time.time >= lastTouchDamageTime + touchDamageCooldown)
        {
            touchDamageBotLeft.Set(touchDamageCheck.position.x - (touchDamageWidth / 2), touchDamageCheck.position.y - (touchDamageHeight / 2));
            touchDamageTopRight.Set(touchDamageCheck.position.x + (touchDamageWidth / 2), touchDamageCheck.position.y + (touchDamageHeight / 2));

            Collider2D hit = Physics2D.OverlapArea(touchDamageBotLeft, touchDamageTopRight, WhatIsPlayer);
            if(hit != null)
            {
                lastTouchDamageTime = Time.time;
                attackDetails[0] = touchDamage;
                attackDetails[1] = Enemy.transform.position.x;
                hit.SendMessage("Damage", attackDetails);
            }
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawLine(groundCheck.position, new Vector2(groundCheck.position.x, groundCheck.position.y - groundCheckDistance));
        Gizmos.DrawLine(wallCheck.position, new Vector2(wallCheck.position.x + wallCheckDistance, wallCheck.position.y));

        Vector2 botLeft = new Vector2(touchDamageCheck.position.x - (touchDamageWidth / 2), touchDamageCheck.position.y - (touchDamageHeight / 2));
        Vector2 botRight = new Vector2(touchDamageCheck.position.x + (touchDamageWidth / 2), touchDamageCheck.position.y - (touchDamageHeight / 2));
        Vector2 topRight = new Vector2(touchDamageCheck.position.x + (touchDamageWidth / 2), touchDamageCheck.position.y + (touchDamageHeight / 2));
        Vector2 topLeft = new Vector2(touchDamageCheck.position.x - (touchDamageWidth / 2), touchDamageCheck.position.y + (touchDamageHeight / 2));

        Gizmos.DrawLine(botLeft, botRight);
        Gizmos.DrawLine(botRight, topRight);
        Gizmos.DrawLine(topRight, topLeft);
        Gizmos.DrawLine(topLeft, botLeft);
    }


}
