﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour
{
    public Text nameText;
    public Text dialogueText;

    private Queue<string> sentences;

    public DialogueTrigger DT;

    public GameObject DialogueWindow;

    private void Start()
    {
        sentences = new Queue<string>();
    }


    public void StartDialogue(objectDialogue dialogue)
    {
        sentences.Clear();

        nameText.text = dialogue.name;

        foreach (string sentence in dialogue.sentences)
        {

            sentences.Enqueue(sentence);
        }

        DisplayNextSentence();
    }

    public void DisplayNextSentence()
    {

        if (sentences.Count == 0)
        {
            endDialogue();
            return;
        }

        string sentence = sentences.Dequeue();
        StopAllCoroutines();
        StartCoroutine(TypeSentense(sentence));
    }

    IEnumerator TypeSentense (string sentence)
    {
        dialogueText.text = "";
        foreach (char letter in sentence.ToCharArray())
        {
            dialogueText.text += letter;
            yield return null;
        }
    }

    void endDialogue()
    {
        GameObject.Find("Player_GFX").GetComponent<PlayerMovementV2>().enabled = true;
        GameObject.Find("Player_GFX").GetComponent<CombatSystem>().enabled = true;
        DialogueWindow.SetActive(false);

    }
}
