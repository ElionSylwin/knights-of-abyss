﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Dialogue : MonoBehaviour
{
    public GameObject image;

    public GameObject DialogueWindow;

    public DialogueTrigger DT;

    private bool windowPossible = false;

    private Animator Anim;

    private void Start()
    {
        Anim = GameObject.Find("Player_GFX").GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if(windowPossible == true && Input.GetKeyDown(KeyCode.E))
        {
            Anim.SetInteger("Movement", 0);
            GameObject.Find("Player_GFX").GetComponent<PlayerMovementV2>().enabled = false;
            GameObject.Find("Player_GFX").GetComponent<CombatSystem>().enabled = false;
            image.SetActive(false);
            DialogueWindow.SetActive(true);
            DT.TriggerDialogue();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //if(collision.tag == "Player")
        //{
            Debug.Log("PLAYER ENTERED");
            image.SetActive(true);
            windowPossible = true;
        //}
        
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        image.SetActive(false);
        windowPossible = false;
    }
}
